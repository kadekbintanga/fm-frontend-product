<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class WebController extends Controller
{
    public function dashboard(Request $request){
        $product = Http::get(env('APP_API_URL').'api/showlistproducts');
        $data['products'] = $product['data'];
        return view('dashboard', $data);

    }
    public function searchResult(Request $request){
        $keyword = $request->input('keyword');
        $category = $request->input('category');
        $product = Http::get(env('APP_API_URL').'api/searchproduct',['keyword'=>$keyword, 'category'=>$category]);
        $data['products'] = $product['data'];
        error_log($keyword."-".$category."-".$product);
        return view('dashboard', $data);
    }
    public function showProduct(Request $request, $id){
        $product = Http::get(env('APP_API_URL').'api/showproduct/'.$id);
        $data['products'] = $product['data'];
        return view('showproduct', $data);
    }
    public function addProduct(Request $request){
        return view('addproduct');
    }

    public function addProductName(Request $request){
        $category = $request->input('category');
        $name = $request->input('name');
        $description = $request->input('description');
        $data = [
            'product_category'=>$category,
            'product_name'=>$name,
            'product_description'=>$description
        ];
        $response = Http::post(env('APP_API_URL').'api/add/product',$data);
        $id = $response['id'];
        return redirect(route("showproduct", $id));
    }

    public function addProductColor(Request $request){
        $color = $request->input('color');
        $product_id = $request->input('product-id');
        error_log("Fet data color: $color and product id: $product_id");
        $response = Http::post(env('APP_API_URL').'api/add/color',[
            'product_color'=>$color,
            'product_id'=>$product_id
        ]);
        $id = $product_id;
        return redirect(route("showproduct", $id));
        
    }
    public function addProductSize(Request $request){
        $size = $request->input('size');
        $price = $request->input('price');
        $product_id = $request->input('product-id');
        $response = Http::post(env('APP_API_URL').'api/add/size',[
            'product_size'=>$size,
            'product_price'=>$price,
            'product_id'=>$product_id
        ]);
        $id = $product_id;
        return redirect(route("showproduct", $id));
        
    }
    public function addProductImage(Request $request){
        $client = new Client(['base_uri' => env('APP_API_URL')]);
        $product_id = $request->input('product-id');

        if ( ($request->file('image')) != NULL || !empty($request->file('image')) ){
            $request->validate([
                'image' => 'mimes:jpeg,bmp,png'
            ]);
            
            $response = $client->request('POST', 'api/add/image', ['multipart' => [
                [
                    'name' => 'product_id',
                    'contents' => $product_id
                ],
                [
                    'name' => 'photo',
                    'contents' => fopen($request->file('image'), 'r')
                ]
            ]]);

        }
        $id = $product_id;
        return redirect(route("showproduct", $id));
    }
    public function deleteProduct(Request $request){
        $product_id = $request->input('product-id');
        $response = Http::post(env('APP_API_URL').'api/deleteproduct',[
            'id'=>$product_id
        ]);
        return redirect(route("dashboard"));
        
    }
}
