@extends('template')
@section('title', 'Dashboard')
@section('content')


<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h2>Name : {{$products['name']}}</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <h4>Category : {{$products['category']}}</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <h4>Color: @foreach ($products['colors'] as $color ){{$color['name']}},@endforeach</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
        @foreach ($products['sizes'] as $size )
            <h4>Size: {{$size['size']}} = Rp{{currencyFormat($size['price'])}}</h4>
        @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col-12">
        @foreach ($products['images'] as $image )
            <img src="{{env('APP_API_URL').$image['image']}}" class="d-block w-100" alt="...">
        @endforeach
        </div>
    </div>
    <div class="row mt-5">
        <h4>Add variant</h4>
    </div>
    <div class="row">
        <div class="col-4">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#colorModal">Add Color</button>
        </div>
        <div class="col-4">
            <button type="button" class="btn btn-primary">Add Size</button>
        </div>
        <div class="col-4">
            <button type="button" class="btn btn-primary">Add Image</button>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="colorModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add new color</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('addproduct.color')}}" role="form" method="post">
            @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Color</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" name="color" placeholder="add new color" />
                        <input type="hidden" name="product-id" class="form-control" value="{{$products['id']}}" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" value="Submit" class="btn btn-primary"/>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection