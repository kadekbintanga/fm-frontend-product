@extends('template')
@section('title', 'Add Product')
@section('content')


<div class="container-fluid">
    <h5 class="mt-3">Add Product</h5>
    <div class="row">
        <div class="col-12">
            <div class="card mt-3">
                <div class="card-header">Product</div>
                <div class="card-body">
                    <form action="{{route('addproduct.product')}}" method = "post" id = "addproduct">
                    @csrf
                        <div class="form-group">
                            <label for="productCategory">Category</label>
                            <select class="form-control" id="productCategory" name="category">
                                <option value="1">Pakaian</option>
                                <option value="2">Celana</option>
                                <option value="3">Topi</option>
                                <option value="4">Jas</option>
                                <option value="5">Sepatu</option>
                                <option value="6">Jaket</option>
                                <option value="7">Tas</option>
                                <option value="8">Dompet</option>
                                <option value="9">Lainnya</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="productName">Name</label>
                            <input type="text" class="form-control" id="productName" name="name">
                        </div>
                        <div class="form-group">
                            <label for="productDescription">Description</label>
                            <textarea class="form-control" id="productDescription" rows="3" name="description"></textarea>
                        </div>
                        <div class="col-12 d-flex justify-content-end">
                            <input type="submit" value="Submit" class="btn btn-primary" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
