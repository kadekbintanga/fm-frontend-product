@extends('template')
@section('title', 'Dashboard')
@section('content')



<!-- Begin Page Content -->
<div class="container-fluid">
    <form action="{{route('searchproduct')}}" role="form" id="searchform" method="post">
    @csrf
        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    <label for="keywordInput">Keyword</label>
                    <input type="text" class="form-control" id="keywordInput" name="keyword" placeholder="Type the keyword">
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label for="categoryInput">Category</label>
                    <select class="form-control" id="categoryInput" name="category">
                        <option value="">All</option>
                        <option>Pakaian</option>
                        <option>Celana</option>
                        <option>Topi</option>
                        <option>Jas</option>
                        <option>Sepatu</option>
                        <option>Jaket</option>
                        <option>Tas</option>
                        <option>Dompet</option>
                        <option>Lainnya</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6 d-flex justify-content-end">
                <input type="submit" value="Search" class="btn btn-primary" />
            </div>
        </div>
    </form>
    <div class="row justify-content-left mt-3">
    @foreach ($products as $product )
        <div class="card mr-2 ml-2 mb-2 mt-2 shadow" style="width: 13rem;">
            <a href="{{route('showproduct',['id' => $product['id']])}}"  style="text-decoration:none; color: inherit;">
                <div class="card-body">
                    <img class="mb-4 mx-auto d-block" src="{{env('APP_API_URL').$product['latestimage']['image']}}" width = "150 px">
                    <h5 class="card-title">{{$product['name']}}</h5>
                    <p class="card-text">{{$product['category']}}</p>
                    <p class="card-text">Rp{{currencyFormat($product['lowestprice']['price'])}} - Rp{{currencyFormat($product['highestprice']['price'])}}</p>
                </div>
            </a>
        </div>
    @endforeach
    </div>
</div>

@endsection
