@extends('template')
@section('title', 'Product Detail')
@section('content')


<div class="container-fluid">
    <div class="row">
        <div class="col-5">
            <div class="row col-12">
                <div id="carouselimage" class="carousel slide rounded" data-ride="carousel"  style="width: 500px; height: 500px; border-radius : 200px; padding: 20px; ">
                    <div class="carousel-inner">
                    @foreach ($products['images'] as $image )
                        <div class="carousel-item {{$loop->first ? 'active' : ''}}">
                            <img src="{{url(env('APP_API_URL').$image['image'])}}" class="d-block w-100  h-100" alt="...">
                        </div>
                    @endforeach
                    </div>
                    <button class="carousel-control-prev" type="button" data-target="#carouselimage" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-target="#carouselimage" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </button>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12 text-center">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#imageModal">add more image</button>
                </div>
            </div>
        </div>
        <div class="col-7">
            <div class="row">
                <div class="col-12">
                    <h2 class="font-weight-bold">{{$products['name']}}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h4>{{$products['category']}}</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row col-12">
                        <h5>Color Variant :</h5>
                    </div>
                    <div class="row col-12">
                        @foreach ($products['colors'] as $color )
                        <h4><span class="badge badge-info mr-1">{{$color['name']}}</span></h4>
                        @endforeach
                    </div>
                    <div class="row col-12">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#colorModal">add more color</button>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <div class="row col-12">
                        <h5>Size and Price Variant :</h5>
                    </div>
                    @foreach ($products['sizes'] as $size )
                    <div class="row col-12">
                        <h4><span class="badge badge-secondary mr-1">Size: {{$size['size']}} = Rp{{currencyFormat($size['price'])}}</span></h4>
                    </div>
                    @endforeach
                    <div class="row col-12">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#sizeModal">add more size and price</button>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <div class="row col-12">
                        <h5>Description :</h5>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    {{$products['description']}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-12">
            <button type="button" class="btn btn-danger btn-lg btn-block" data-toggle="modal" data-target="#deleteModal">Delete product</button>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="colorModal" tabindex="-1" aria-labelledby="ModalColor" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalColor">Add new color</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('addproduct.color')}}" role="form" method="post">
            @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="colorInput">Color</label>
                        <input type="text" class="form-control" id="colorInput" name="color" placeholder="add new color" />
                        <input type="hidden" name="product-id" class="form-control" value="{{$products['id']}}" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" value="Submit" class="btn btn-primary"/>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="sizeModal" tabindex="-1" aria-labelledby="ModalSize" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalSize">Add new size and price</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('addproduct.size')}}" role="form" method="post">
            @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="sizeInput">Size</label>
                        <input type="text" class="form-control" id="sizeInput" name="size" placeholder="add new size" />
                        <label class="mt-3" for="priceInput">Price</label>
                        <input type="text" class="form-control" id="priceInput" name="price" placeholder="example : 50000 -> dont add (.) or (,)" />
                        <input type="hidden" name="product-id" class="form-control" value="{{$products['id']}}" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" value="Submit" class="btn btn-primary"/>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="imageModal" tabindex="-1" aria-labelledby="ModalImage" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalImage">Add new image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('addproduct.image')}}" role="form" method="post" enctype="multipart&#x2F;form-data">
            @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="imageInput">Input image</label>
                        <input type="file" class="form-control-file" id="imageInput" name="image"/>
                        <input type="hidden" name="product-id" class="form-control" value="{{$products['id']}}" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" value="Submit" class="btn btn-primary"/>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detele product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('deleteproduct')}}" role="form" method="post" enctype="multipart&#x2F;form-data">
            @csrf
                <div class="modal-body">
                    <p>Do you want to delete product {{$products['name']}}...?</p>
                    <div class="form-group">
                        <input type="hidden" name="product-id" class="form-control" value="{{$products['id']}}" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <input type="submit" value="Yes" class="btn btn-primary"/>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection