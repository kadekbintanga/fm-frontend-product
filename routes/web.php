<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['middleware' => 'web'], function () {
    Route::get('/', 'WebController@dashboard')->name('dashboard');
    Route::get('showproduct/{id}', 'WebController@showProduct')->name('showproduct');
    Route::post('searchproduct', 'WebController@searchResult')->name('searchproduct');
    Route::get('addproduct', 'WebController@addProduct')->name('addproduct');
    Route::post('addproduct/product', 'WebController@addProductName')->name('addproduct.product');
    Route::post('addproduct/color', 'WebController@addProductColor')->name('addproduct.color');
    Route::post('addproduct/size', 'WebController@addProductSize')->name('addproduct.size');
    Route::post('addproduct/image', 'WebController@addProductImage')->name('addproduct.image');
    Route::post('deleteproduct', 'WebController@deleteProduct')->name('deleteproduct');
});
